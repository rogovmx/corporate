require 'test_helper'

class MailerTest < ActionMailer::TestCase
  test "new_user" do
    @expected.subject = 'Mailer#new_user'
    @expected.body    = read_fixture('new_user')
    @expected.date    = Time.now

    assert_equal @expected.encoded, Mailer.create_new_user(@expected.date).encoded
  end

end
