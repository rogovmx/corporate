require 'test_helper'

class MenuL2sControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:menu_l2s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create menu_l2" do
    assert_difference('MenuL2.count') do
      post :create, :menu_l2 => { }
    end

    assert_redirected_to menu_l2_path(assigns(:menu_l2))
  end

  test "should show menu_l2" do
    get :show, :id => menu_l2s(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => menu_l2s(:one).to_param
    assert_response :success
  end

  test "should update menu_l2" do
    put :update, :id => menu_l2s(:one).to_param, :menu_l2 => { }
    assert_redirected_to menu_l2_path(assigns(:menu_l2))
  end

  test "should destroy menu_l2" do
    assert_difference('MenuL2.count', -1) do
      delete :destroy, :id => menu_l2s(:one).to_param
    end

    assert_redirected_to menu_l2s_path
  end
end
