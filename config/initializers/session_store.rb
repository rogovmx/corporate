# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_zo_corporate_session',
  :secret      => 'a404fecedc1a8fbacb24d00c5439d8b7f2465d16d3eaa35f46028e64c0874fddc44a069f9767e300bc568216e65f6bd2c2d5ea99f3e6b46c036702b8c5e03b2e'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
 ActionController::Base.session_store = :active_record_store
