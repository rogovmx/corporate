class CreateSpams < ActiveRecord::Migration
  def self.up
    create_table :spams do |t|
      t.text :cont
      t.boolean :delivered

      t.timestamps
    end
  end

  def self.down
    drop_table :spams
  end
end
