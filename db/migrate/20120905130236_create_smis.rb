class CreateSmis < ActiveRecord::Migration
  def self.up
    create_table :smis do |t|
      t.string :title
      t.text :content
      t.string :magazin
      t.timestamps
    end
  end

  def self.down
    drop_table :smis
  end
end
