class CreateMenuL3s < ActiveRecord::Migration
  def self.up
    create_table :menu_l3s do |t|
      t.string :title
      t.string :url
      t.text :content
      t.string :modul
      t.boolean :vis
      t.integer :menu_l2_id

      t.timestamps
    end
  end

  def self.down
    drop_table :menu_l3s
  end
end
