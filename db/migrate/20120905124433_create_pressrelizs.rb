class CreatePressrelizs < ActiveRecord::Migration
  def self.up
    create_table :pressrelizs do |t|
      t.string :title
      t.text :notice
      t.text :content
      t.string :magazin

      t.timestamps
    end
  end

  def self.down
    drop_table :pressrelizs
  end
end
