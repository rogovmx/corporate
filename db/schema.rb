# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130712113907) do

  create_table "bookings", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "actions_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                                 :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 25
    t.string   "guid",              :limit => 10
    t.integer  "locale",            :limit => 1,  :default => 0
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "fk_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_assetable_type"
  add_index "ckeditor_assets", ["user_id"], :name => "fk_user"

  create_table "confsubs", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "consultations", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feedbacks", :force => true do |t|
    t.integer  "user_id"
    t.text     "message"
    t.string   "mail"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "index1s", :force => true do |t|
    t.text     "content"
    t.text     "content_en"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "marketing_consultations", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "menu_l1s", :force => true do |t|
    t.string   "title"
    t.string   "url"
    t.text     "content"
    t.string   "title_en"
    t.string   "url_en"
    t.text     "content_en"
    t.boolean  "vis",        :default => true, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order"
  end

  create_table "menu_l2s", :force => true do |t|
    t.string   "title"
    t.string   "url"
    t.text     "content",    :limit => 2147483647
    t.string   "modul"
    t.string   "title_en"
    t.string   "url_en"
    t.text     "content_en", :limit => 2147483647
    t.string   "modul_en"
    t.boolean  "vis",                              :default => true, :null => false
    t.integer  "ord",                              :default => 1
    t.integer  "menu_l1_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "menu_l3s", :force => true do |t|
    t.string   "title"
    t.string   "url"
    t.text     "content"
    t.string   "modul"
    t.string   "title_en"
    t.string   "url_en"
    t.text     "content_en"
    t.string   "modul_en"
    t.boolean  "vis",        :default => true, :null => false
    t.integer  "menu_l2_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news_items", :force => true do |t|
    t.string   "title"
    t.text     "notice"
    t.text     "content"
    t.string   "title_en"
    t.text     "notice_en"
    t.text     "content_en"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photogroups", :force => true do |t|
    t.string   "title"
    t.text     "notice"
    t.string   "title_en"
    t.text     "notice_en"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photos", :force => true do |t|
    t.integer  "photogroup_id"
    t.string   "prw"
    t.string   "img"
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pressrelizs", :force => true do |t|
    t.string   "title"
    t.text     "notice"
    t.text     "content"
    t.string   "magazin"
    t.string   "title_en"
    t.text     "notice_en"
    t.text     "content_en"
    t.string   "magazin_en"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "smis", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.string   "magazin"
    t.string   "title_en"
    t.text     "content_en"
    t.string   "magazin_en"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "magazin_link"
    t.text     "notice"
    t.text     "notice_en"
  end

  create_table "spams", :force => true do |t|
    t.string   "title"
    t.text     "cont"
    t.boolean  "delivered"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subscribes", :force => true do |t|
    t.string   "mail"
    t.string   "conf"
    t.boolean  "spam"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "mail"
    t.string   "fname"
    t.string   "pname"
    t.string   "sname"
    t.string   "phone"
    t.string   "company"
    t.string   "hashed_password"
    t.string   "salt"
    t.integer  "email_confirm"
    t.boolean  "spam"
    t.string   "role"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
