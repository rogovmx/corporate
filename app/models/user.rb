require 'digest/sha1'

class User < ActiveRecord::Base
  
  validates_format_of       :mail ,
                            :with=>/\A([-a-z0-9!\#$%&'*+\/=?^_`{|}~]+\.)*[-a-z0-9!\#$%&'*+\/=?^_`{|}~]+@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i ,
                            :message=>"Неправильный адрес эл.почты."
  validates_presence_of     :mail,
                            :message=>"Поле E-mail(логин) не может быть пустым."
  validates_uniqueness_of   :mail ,
                            :message=>"Такой E-mail уже зарегистрирован. Попробуйте другой или свяжитесь с поддержкой"
  
  
  attr_accessor             :password_confirmation
  validates_confirmation_of :password ,:message=>"Пароли не совпадают."
  
 #validates_presence_of     :mail,:message=>"Поле Электронная почта не может быть пустым."
   
  validates_presence_of     :fname,:message=>"Поле Имя не может быть пустым."

  validates_presence_of     :company,:message=>"Поле Название организации не может быть пустым."
  validates_length_of :password, :in => 5..12 ,:allow_nil => true, :too_long => 'Пароль должен быть не больше 12 символов.', :too_short => 'Пароль должен быть не меньше 5 символов.'

  def validate
    errors.add_to_base("Пустой пароль") if hashed_password.blank?
  end
  
  
  
    def self.authenticate(mail, password)

    user = self.find_by_mail(mail)
    if user
    expected_password = encrypted_password(password, user.salt)
    user = nil  if user.hashed_password != expected_password
    end
    user

    end

  #END:login

  # 'password' is a virtual attribute
  #START:accessors
  def password
    @password
  end

  def password=(pwd)
    @password = pwd
    create_new_salt
    self.hashed_password = User.encrypted_password(self.password, self.salt)
  end
  #END:accessors

  #START:after_destroy
  def after_destroy
    if User.count.zero?
      raise "Can't delete last user"
    end
  end
  #END:after_destroy

  private

  #START:create_new_salt
  def create_new_salt
    self.salt = self.object_id.to_s + rand.to_s
  end
  #END:create_new_salt

  #START:encrypted_password
  def self.encrypted_password(password, salt)
    string_to_hash = password + "bubble" + salt
    Digest::SHA1.hexdigest(string_to_hash)
  end
  
  
end
