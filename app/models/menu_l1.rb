class MenuL1 < ActiveRecord::Base
  has_many :menu_l2
  named_scope :visible, :conditions => { :vis => 1 }
end
