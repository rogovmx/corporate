class MarketingConsultation < ActiveRecord::Base
	VALID_EMAIL = /\A([-a-z0-9!\#$%&'*+\/=?^_`{|}~]+\.)*[-a-z0-9!\#$%&'*+\/=?^_`{|}~]+@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  validates_presence_of :name, :message=>"Поле ФИО не может быть пустым."
  validates_presence_of :email, :message=>"Поле E-mail не может быть пустым."
  validates_format_of   :email ,
                        :with => VALID_EMAIL,
                        :message => "Неправильный адрес эл.почты."
  validates_presence_of :phone, :message=>"Поле Телефон не может быть пустым."
end