class Mailer < ActionMailer::Base
  

  def spam(spam, mail , conf)
    
    content_type 'text/html'
    subject    spam.title
    recipients mail
    from      'Корпоративный сайт живой офис <corporate@zhivojoffice.ru>'
    sent_on    Time.now
    body      :cont=>spam.cont , :conf=>conf


  end


  def welcome(user, pass)

    content_type 'text/html'
    subject    'Спасибо за регистрацию!'
    recipients user.mail
    from      'Корпоративный сайт живой офис <welcome@zhivojoffice.ru>'
    sent_on    Time.now
    body      :user=>user ,:pass=>pass
  end



  def new1(user, pass)
    content_type 'text/html'
    subject    'Регистрация на корпоративном сайте'
    recipients ['mrogov@spens.ru']

    from       'Корпоративный сайт живой офис <welcome@zhivojoffice.ru>'
    sent_on   
    body      :user=>user ,:pass=>pass
  end
  


  def pass(mail,name,rnd)
 # def welcome(mail,rnd)
    subject    'Сервис изменения пароля.'
    recipients [mail,'support1@zhivojoffice.ru']
    from      'welcome@zhivojoffice.ru'
    sent_on    Time.now
    body      :rnd=>rnd,:mail=>mail,:name=>name

  end

def subs(subs)
    content_type 'text/html'
    subject    'Подписка на рассылку с корпоративного сайта Живой офис'
    recipients [subs.mail]

    from       'Корпоративный сайт живой офис <welcome@zhivojoffice.ru>'
    sent_on
    body      :subs=>subs

end


def feedback(fb)
     @user=User.find_by_id(fb.user_id) if fb.user_id

     content_type 'text/html'
     subject    'Обратная связь. Корпоративный сайт Живой офис'
     recipients ['mrogov@spens.ru' , 'pr@zhivojoffice.ru', 'aulanov@spens.ru', 'izenkova@spens.ru', 'iruleva@spens.ru']
     from      'Сайт Живой офис  <welcome@zhivojoffice.ru>'
     sent_on    Time.now
     body["feedback"]=fb

  end





def mailer2(user)

content_type 'text/html'
subject  '«Живой офис»: горячие АКЦИИ – июнь 2012'
recipients   user
from  'Сайт Живой офис  <welcome@zhivojoffice.ru>'
sent_on  Time.now
body

end


def mailer(user)

content_type 'multipart/related'
subject  'Не пропустите! Новый конкурс от Живого офиса и много приятных акций на нашем сайте в августе!'
recipients   user
from  'welcome@zhivojoffice.ru'
sent_on  Time.now


part :content_type => 'text/html',
:body => '<a href="http://zhivojoffice.ru"><img src="cid:part1@domain.com" border="0"></a><div>
<p>Готовьте и выигрывайте! В "Живом офисе" стартует новый, кулинарный <a href="http://zhivojoffice.ru/info/contest">конкурс!</a></p>
  <p>Принимайте участие в наших <a href="http://zhivojoffice.ru/act">акциях</a> и получайте подарки!</p>',
:content_disposition => 'inline',
:headers => { 'content-id' => '<html-1@domain.com>' }

part :content_type => 'image/gif',
:content_disposition => 'inline',
:transfer_encoding => 'base64',
:body => File.read(RAILS_ROOT+"/public/images2/1_08_msk.gif"),
:filename => '1_08_msk.gif',
:headers => { 'content-id' => '<part1@domain.com>' }
end

  def actions_booking(booking)
    content_type     'text/html'
    subject          'Заявка на приобретение акций ОАО «Живой офис»'
    @name =           booking.name
    @email =          booking.email
    @phone =          booking.phone
    @actions_number = booking.actions_number

    recipients        ['pr@zhivojoffice.ru', 'iruleva@spens.ru', 'izenkova@spens.ru', 'gpetrova@spens.ru', 'aulanov@spens.ru', 'mrogov@spens.ru']
    from              'Сайт Живой офис <welcome@zhivojoffice.ru>'
    sent_on           Time.now
  end

  def actions_booking_response(booking)
    content_type     'text/html'
    subject          'Заявка на приобретение акций ОАО «Живой офис»'
    @name =           booking.name
    @email =          booking.email

    recipients        "#{@email}"
    from              'Сайт Живой офис <welcome@zhivojoffice.ru>'
    sent_on           Time.now
  end

  def ipo_consultation(consultation)
    content_type     'text/html'
    subject          'Запрос на получение консультации специалистов ОАО «Живой офис»'
    @name =           consultation.name
    @email =          consultation.email
    @phone =          consultation.phone

    recipients        ['pr@zhivojoffice.ru', 'iruleva@spens.ru', 'gpetrova@spens.ru', 'aulanov@spens.ru', 'mrogov@spens.ru', 'izenkova@spens.ru']
    from              'Сайт Живой офис <welcome@zhivojoffice.ru>'
    sent_on           Time.now
  end

  def ipo_consultation_response(consultation)
    content_type     'text/html'
    subject          'Запрос на получение консультации специалистов ОАО «Живой офис»'
    @name =           consultation.name
    @email =          consultation.email

    recipients        "#{@email}"
    from              'Сайт Живой офис <welcome@zhivojoffice.ru>'
    sent_on           Time.now
  end

  def marketing_consultation(consultation)
    content_type     'text/html'
    subject          'Запрос на получение консультации специалистов ОАО «Живой офис»'
    @name =           consultation.name
    @email =          consultation.email
    @phone =          consultation.phone

    recipients        ['izenkova@spens.ru', 'aulanov@spens.ru', 'mrogov@spens.ru', 'iruleva@spens.ru'] 
    from              'Сайт Живой офис <welcome@zhivojoffice.ru>'
    sent_on           Time.now
  end

  def marketing_consultation_response(consultation)
    content_type     'text/html'
    subject          'Запрос на получение консультации специалистов ОАО «Живой офис»'
    @name =           consultation.name
    @email =          consultation.email

    recipients        "#{@email}"
    from              'Сайт Живой офис <welcome@zhivojoffice.ru>'
    sent_on           Time.now
  end


#'bilenko@eastlandcapital.com' , 'osadchiy@eastlandcapital.com',
end
