class PressrelizsController < ApplicationController
before_filter :authorize
before_filter :redaktor
layout 'admin'

  def index
   @menu = MenuL1.all
    @page_title = "Администрирование. Пресс-релизы"
    @pressrelizs =  Pressreliz.find :all ,:order=>'id desc'

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @pressrelizs }
    end
  end

  # GET /pressrelizs/1
  # GET /pressrelizs/1.xml
  def show
    @menu = MenuL1.all
    @page_title = "Администрирование. Пресс-релизы"
    @pressreliz = Pressreliz.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @pressreliz }
    end
  end

  # GET /pressrelizs/new
  # GET /pressrelizs/new.xml
  def new
    @menu = MenuL1.all
    @page_title = "Администрирование. Пресс-релизы"
    @pressreliz = Pressreliz.new
    @title = "Пресс-релиз: #{@pressreliz.title}"

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @pressreliz }
    end
  end

  # GET /pressrelizs/1/edit
  def edit
    @menu = MenuL1.all
    @page_title = "Администрирование. Пресс-релизы"
    @pressreliz = Pressreliz.find(params[:id])
  end

  # POST /pressrelizs
  # POST /pressrelizs.xml
  def create
    @pressreliz = Pressreliz.new(params[:pressreliz])

    respond_to do |format|
      if @pressreliz.save
        format.html { redirect_to(@pressreliz, :notice => 'Пресс-релиз создан.') }
        format.xml  { render :xml => @pressreliz, :status => :created, :location => @pressreliz }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @pressreliz.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /pressrelizs/1
  # PUT /pressrelizs/1.xml
  def update
    @pressreliz = Pressreliz.find(params[:id])

    respond_to do |format|
      if @pressreliz.update_attributes(params[:pressreliz])
        format.html { redirect_to(@pressreliz, :notice => 'Пресс-релиз сохранен.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @pressreliz.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /pressrelizs/1
  # DELETE /pressrelizs/1.xml
  def destroy
    @pressreliz = Pressreliz.find(params[:id])
    @pressreliz.destroy

    respond_to do |format|
      format.html { redirect_to(pressrelizs_url) }
      format.xml  { head :ok }
    end
  end
end
