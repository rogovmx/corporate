class SubsController < ApplicationController
  def subs_mail

    @subs = Subscribe.new
    @subs.mail = params[:mail][:mail]
    @subs.spam = 1
    @subs.conf = rand(999999999999999999999999999999999999999999)
    @exist = Subscribe.find_by_mail(params[:mail][:mail])
    @str = /\A([-a-z0-9!\#$%&'*+\/=?^_`{|}~]+\.)*[-a-z0-9!\#$%&'*+\/=?^_`{|}~]+@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
    if @exist != nil
    @exist.spam = 1
    @exist.save
    render :text => '<b>Вы уже подписаны на рассылку: ' + params[:mail][:mail] + '</b>'
    elsif params[:mail][:mail] =~ @str and @subs.save
    Mailer.deliver_subs(@subs)
    render :text => '<b>Вы подписаны на рассылку: ' + params[:mail][:mail] + '</b>'
    else
    @mess='Неправильный E-mail'
    render :partial => '/part/subs' ,:mess=>@mess
    end
  end
  
  def reject
    @ex = Subscribe.find_by_conf(params[:reject])
    if @ex
    @ex.spam = 0
    @ex.save
    end

    flash[:notice] = "#{@ex.mail} исключен из рассылки"
    redirect_to :controller=>'main'
  end
  

end
