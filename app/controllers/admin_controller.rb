class AdminController < ApplicationController

before_filter :authorize
before_filter :redaktor

  def index
    @menu = MenuL1.all
    @title = "Дерево сайта."
    @page_title = "Администрирование. Корпоративный сайт группы компаний «Живой офис»"
    if params[:id]
    @edit_menu = MenuL1.find(:all , :conditions=>['id=?',params[:id]])
    @title += " Меню #{@edit_menu[0].title}"
    else
    @edit_menu = MenuL1.all
    end
  end

  def main_page
    @menu = MenuL1.all
    @page_title = "Администрирование. Главная страница"
    @content = Index1.find(1).content
    @index = Index1.find(1)
  end

  def save_index
    
    @index = Index1.find(1)
    if @index.update_attributes(params[:index])
     # render :text => params[:index]
      redirect_to(:action=>'index', :notice => 'Главная страница сохранена')
    end

  end



end
