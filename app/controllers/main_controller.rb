class MainController < ApplicationController
  layout 'main'




def index
    user
   ## if request.xhr? @headers["Content-Type"] = "text/javascript; charset=utf-8" else @headers["Content-Type"] = "text/html; charset=utf-8"
   ## headers["Content-Type"] = "text/html; charset=utf-8"

    @menu = MenuL1.visible.sort{|a, b| a.order <=> b.order}
    @mp = 1

    if ru?
      @page_title = "Корпоративный сайт группы компаний «Живой офис»"
      @content = Index1.find(1).content
    else
      @page_title = "Live Office Group"
      @content = Index1.find(1).content_en
    end
    @press = Pressreliz.find :last
    @smi = Smi.find :last
    @news_item = NewsItem.find :last


    render :layout => 'index'


# redirect_to "/%D0%9E%20%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8"
end



def page
    user
    begin
    @menu = MenuL1.visible.sort{|a, b| a.order <=> b.order}
    @menu.map!{|x| x if x.menu_l2.size != 0}
    @act_menu = MenuL1.find(:first , :conditions=>['url = ? or url_en = ?', params[:str], params[:str]] )
    @act_menu_l2 = @act_menu.menu_l2.sort{|a, b| a.ord <=> b.ord}.map{|x| x if x.vis}[0]

    if @act_menu_l2.menu_l3.size != 0
    @act_menu_l3 = @act_menu_l2.menu_l3.map{|x| x if x.vis}[0]
    @content = @act_menu_l3
    @page_title = @act_menu_l3.title if ru?
    @page_title = @act_menu_l3.title_en unless ru?
    else
    @content = @act_menu_l2
    @page_title = @act_menu_l2.title if ru?
    @page_title = @act_menu_l2.title_en unless ru?
    end

    if params[:str2] #and (params[:str3] == nil)
    @act_menu_l2 = MenuL2.find(:first , :conditions=>['url = ? or url_en = ? and vis = ? and menu_l1_id = ?', params[:str2], params[:str2] ,1 , @act_menu.id])

      if @act_menu_l2.menu_l3.map{|x| x if x}.size != 0
      @act_menu_l3 = @act_menu_l2.menu_l3.map{|x| x if x.vis}[0]
      @content = @act_menu_l3
      @page_title = @act_menu.title + '. ' + @act_menu_l2.title + '. ' + @act_menu_l3.title if ru?
      @page_title = @act_menu.title_en + '. ' + @act_menu_l2.title_en + '. ' + @act_menu_l3.title_en unless ru?
      else
      @content = @act_menu_l2
      @page_title = @act_menu.title + '. ' + @act_menu_l2.title if ru?
      @page_title = @act_menu.title_en + '. ' + @act_menu_l2.title_en unless ru?
      end
    end

    if params[:str3]
    @act_menu_l3 = MenuL3.find(:first , :conditions=>['url = ? or url_en = ? and vis = ? and menu_l2_id = ?', params[:str3] , params[:str3], 1 , @act_menu_l2.id])

    @content = @act_menu_l3
    @page_title = @act_menu.title + '. ' + @act_menu_l2.title + '. ' + @act_menu_l3.title if ru?
    @page_title = @act_menu.title_en + '. ' + @act_menu_l2.title_en + '. ' + @act_menu_l3.title_en unless ru?
    end
    rescue
    redirect_to '/'
    end

end

def sitetree
  user
  @menu = MenuL1.visible.sort{|a, b| a.order <=> b.order}
  @page_title ="Корпоративный сайт группы компаний «Живой офис». Карта сайта " if ru?
  @page_title ="Live Office Group. Site map " unless ru?
  @title = "Карта сайта" if ru?
  @title = "Site map" unless ru?
  render :layout => 'index'
end


def feedback

  @feedback = Feedback.new(params[:feedback])
  if @feedback.save
  Mailer.deliver_feedback(@feedback)
  flash[:notice] = 'Ваше сообщение отправлено' if ru?
  flash[:notice] = 'Done' unless ru?

  render :partial=>'/modul/feedback'
  end
end


def press
user
@menu = MenuL1.visible.sort{|a, b| a.order <=> b.order}
@page_title ="Корпоративный сайт группы компаний «Живой офис». Пресс-релизы " if ru?
@page_title ="Live Office Group. Press releases. " unless ru?
@press = Pressreliz.find_by_id(params[:id])
if @press
@act_menu = MenuL1.find(4)
@act_menu_l2 = MenuL2.find(:first , :conditions=>['url = ?', 'press-releases'])
@title = "Пресс-релиз: #{@press.title}" if ru?
@title = "Press releas: #{@press.title_en}" unless ru?
else
redirect_to '/press/press-releases'
end
end

def smi
  user
  @menu = MenuL1.visible.sort{|a, b| a.order <=> b.order}
  @page_title ="Корпоративный сайт группы компаний «Живой офис». СМИ о компании " if ru?
  @page_title ="Live Office Group. Media about company " unless ru?
  @smi = Smi.find_by_id(params[:id])
  if @smi
    @act_menu = MenuL1.find(4)
    @act_menu_l2 = MenuL2.find(:first , :conditions=>['url = ?', 'СМИ о компании'])
    @title = "СМИ о компании: #{@smi.title}" if ru?
    @title = "Media about company: #{@smi.title_en}" unless ru?
  else
    redirect_to '/press/media'
  end
end

def news_item
  user
  @menu = MenuL1.visible.sort{|a, b| a.order <=> b.order}
  @page_title ="Корпоративный сайт группы компаний «Живой офис». Новости компании "
  @news_item = NewsItem.find_by_id(params[:id])
  @act_menu = MenuL1.find(4)
  @act_menu_l2 = MenuL2.find(:first , :conditions=>['url = ?', 'news_item'])

  @title = "Новости компании: #{@news_item.title}"
end

def chng_lang
  session[:lang] = params[:lang]
  redirect_to :back
end

  def ipo_zhivojoffice
    if request.post? && params[:booking]
      @booking = Booking.new(params[:booking])
      if @booking.save
        flash[:notice] = 'Ваша заявка принята.'
        Mailer.deliver_actions_booking(@booking)
        Mailer.deliver_actions_booking_response(@booking)
      else
        flash[:notice] = 'Ошибка. Пожалуйста заполните все поля, помеченные *. Проверьте правильность E-mail.'
      end
      redirect_to :back
    elsif request.post? && params[:consultation]
      @consultation = Consultation.new(params[:consultation])
      if @consultation.save
        flash[:notice] = 'Ваша заявка принята.'
        Mailer.deliver_ipo_consultation(@consultation)
        Mailer.deliver_ipo_consultation_response(@consultation)
      else
        flash[:notice] = 'Ошибка. Пожалуйста заполните все поля, помеченные *. Проверьте правильность E-mail.'
      end
      redirect_to :back
    elsif request.post? && params[:marketing_consultation]
      @marketing_consultation = MarketingConsultation.new(params[:marketing_consultation])
      if @marketing_consultation.save
        flash[:notice] = 'Ваша заявка принята.'
        Mailer.deliver_marketing_consultation(@marketing_consultation)
        Mailer.deliver_marketing_consultation_response(@marketing_consultation)
      else
        flash[:notice] = 'Ошибка. Пожалуйста заполните все поля, помеченные *. Проверьте правильность E-mail.'
      end
      redirect_to :back
    end

  end

end
