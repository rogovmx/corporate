class SmisController < ApplicationController
before_filter :authorize
before_filter :redaktor
layout 'admin'

  def index
    @smis = Smi.find :all ,:order=>'id desc'
    @menu = MenuL1.all
    @page_title = "Администрирование. СМИ о компании"
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @smis }
    end
  end

  # GET /smis/1
  # GET /smis/1.xml
  def show
    @menu = MenuL1.all
    @page_title = "Администрирование. СМИ о компании"
    @smi = Smi.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @smi }
    end
  end

  # GET /smis/new
  # GET /smis/new.xml
  def new
    @menu = MenuL1.all
    @page_title = "Администрирование. СМИ о компании"
    @smi = Smi.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @smi }
    end
  end

  # GET /smis/1/edit
  def edit
    @menu = MenuL1.all
    @page_title = "Администрирование. СМИ о компании"
    @smi = Smi.find(params[:id])
  end

  # POST /smis
  # POST /smis.xml
  def create
    @smi = Smi.new(params[:smi])

    respond_to do |format|
      if @smi.save
        format.html { redirect_to(@smi, :notice => 'Статья добавлена.') }
        format.xml  { render :xml => @smi, :status => :created, :location => @smi }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @smi.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /smis/1
  # PUT /smis/1.xml
  def update
    @smi = Smi.find(params[:id])

    respond_to do |format|
      if @smi.update_attributes(params[:smi])
        format.html { redirect_to(@smi, :notice => 'Статья сохранена.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @smi.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /smis/1
  # DELETE /smis/1.xml
  def destroy
    @smi = Smi.find(params[:id])
    @smi.destroy

    respond_to do |format|
      format.html { redirect_to(smis_url) }
      format.xml  { head :ok }
    end
  end
end
